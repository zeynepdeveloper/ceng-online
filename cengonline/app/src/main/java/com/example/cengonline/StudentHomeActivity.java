package com.example.cengonline;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class StudentHomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_home);
    }
    public void fn(View view) {

        /*

        switch(view.getId())
        {
            case R.id.courseButton:
            {
                Intent intent=new Intent(StudentHomeActivity.this,EnrolledCourseView.class);
                startActivity(intent);
                break;
            }
            case R.id.chatButton:
            {
                Intent intent3=new Intent(StudentHomeActivity.this,ChatActivity.class);
                startActivity(intent3);
                break;
            }
            case R.id.postButton:
            {
                Intent intent2=new Intent(StudentHomeActivity.this,CrudCourseStudentPostOperation.class);
                startActivity(intent2);
                break;
            }
            case R.id.studentButton:
            {
                Intent intent4=new Intent(StudentHomeActivity.this,CrudCourseStudentPostOperation.class);
                startActivity(intent4);

            }

        }
        */




        if(view.getId()==R.id.courseButton){
            // Toast.makeText(MainActivity.this, "Hello", Toast.LENGTH_SHORT).show();
            Intent intent=new Intent(StudentHomeActivity.this,EnrolledCourseView.class);
            startActivity(intent);
        }
        else if(view.getId()==R.id.chatButton){
            Intent intent3=new Intent(StudentHomeActivity.this,ChatActivity.class);
            startActivity(intent3);
        }
        else if(view.getId()==R.id.postButton){
            Intent intent3=new Intent(StudentHomeActivity.this,CrudCourseStudentPostOperation.class);
            startActivity(intent3);
        }
        else{
            Intent intent2=new Intent(StudentHomeActivity.this,CrudAnnouncementViewOperation.class);
            startActivity(intent2);
        }


    }

    private class MyListener implements DialogInterface.OnClickListener {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(0);
        }
    }
}
