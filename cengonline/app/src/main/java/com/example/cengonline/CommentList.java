package com.example.cengonline;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

//import android.support.annotation.NonNull;


public class CommentList extends ArrayAdapter<Comment> {
    private Activity context;
    List<Comment> comments;

    public CommentList(Activity context, List<Comment> comments) {
        super(context, R.layout.layout_comment_list, comments);
        this.context = context;
        this.comments = comments;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.layout_submitted_work_list, null, true);

        TextView textViewName = (TextView) listViewItem.findViewById(R.id.textViewName);
        TextView textViewGenre = (TextView) listViewItem.findViewById(R.id.textViewInfo);

        Comment comment = comments.get(position);
        textViewName.setText(comment.getName());
        textViewGenre.setText(comment.getInfo());

        return listViewItem;
    }
}