package com.example.cengonline;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class EnrolledCourseAssignmentView extends AppCompatActivity {

    public static final String ASSIGNMENT_NAME = "com.example.cengonline.assignmentname";
    public static final String ASSIGNMENT_ID = "com.example.cengonline.assignmentid";


    EditText editTextAssignmentName, editTextAssignmentInfo, editTextAssignmentDueDate;

    TextView  textViewCourse;
    ListView listViewAssignments;

    DatabaseReference databaseAssignments;

    List<Assignment> assignments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enrolled_assignment_view);

        Intent intent = getIntent();

        /*
         * this line is important
         * this time we are not getting the reference of a direct node
         * but inside the node assignment we are creating a new child with the course id
         * and inside that node we will store all the assignments with unique ids
         * */
        databaseAssignments = FirebaseDatabase.getInstance().getReference("assignments").child(intent.getStringExtra(EnrolledCourseTeacherView.COURSE_ID));


        editTextAssignmentName = (EditText) findViewById(R.id.editTextName);
        editTextAssignmentInfo = (EditText) findViewById(R.id.editTextInfo);
        editTextAssignmentDueDate = (EditText) findViewById(R.id.editTextDueDate);

        textViewCourse = (TextView) findViewById(R.id.textViewCourse);
        listViewAssignments = (ListView) findViewById(R.id.listViewAssignments);

        assignments = new ArrayList<>();

        textViewCourse.setText(intent.getStringExtra(CrudOperation.COURSE_NAME));



        listViewAssignments.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //getting the selected assignment
                Assignment assignment = assignments.get(i);

                //creating an intent
                Intent intent = new Intent(getApplicationContext(), SubmittedWorkViewActivity.class);

                //putting assignment name and id to intent
                intent.putExtra(ASSIGNMENT_ID, assignment.getId());
                intent.putExtra(ASSIGNMENT_NAME , assignment.getName());

                //starting the activity with intent
                startActivity(intent);
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();

        databaseAssignments.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                assignments.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Assignment assignment = postSnapshot.getValue(Assignment.class);
                    assignments.add(assignment);
                }
                AssignmentList assignmentListAdapter = new AssignmentList(EnrolledCourseAssignmentView.this, assignments);
                listViewAssignments.setAdapter(assignmentListAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }













}