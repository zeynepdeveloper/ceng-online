package com.example.cengonline;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class EnrolledStudentList extends ArrayAdapter<EnrolledStudent> {
    private Activity context;
    List<EnrolledStudent> students;

    public EnrolledStudentList(Activity context, List<EnrolledStudent> students) {
        super(context, R.layout.layout_enrolled_student_list, students);
        this.context = context;
        this.students = students;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.layout_enrolled_student_list, null, true);


        TextView textViewName = (TextView) listViewItem.findViewById(R.id.textViewName);
        TextView textViewEmail = (TextView) listViewItem.findViewById(R.id.textViewEmail);

        EnrolledStudent student = students.get(position);
        textViewName.setText(student.getFullname());
        textViewEmail.setText(student.getEmail());

        return listViewItem;
    }
}