package com.example.cengonline;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class CourseStudentCommentActivity extends AppCompatActivity {
    public static final String COMMENT_NAME = "com.example.cengonline.commentname";
    public static final String COMMENT_ID = "com.example.cengonline.commentid";


    Button buttonAddComment;
    EditText editTextCommentName;
    EditText editTextCommentInfo;
    TextView  textViewCourse;
    ListView listViewComments;

    DatabaseReference databaseComments;

    List<Comment> comments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_comment);

        Intent intent = getIntent();

        /*
         * this line is important
         * this time we are not getting the reference of a direct node
         * but inside the node comment we are creating a new child with the course id
         * and inside that node we will store all the comments with unique ids
         * */
        databaseComments = FirebaseDatabase.getInstance().getReference("comments").child(intent.getStringExtra(CourseStudentPostActivity.POST_ID));

        buttonAddComment = (Button) findViewById(R.id.buttonAddComment);
        editTextCommentName = (EditText) findViewById(R.id.editTextName);
        editTextCommentInfo = (EditText) findViewById(R.id.editTextInfo);
        textViewCourse = (TextView) findViewById(R.id.textViewCourse);
        listViewComments = (ListView) findViewById(R.id.listViewComments);

        comments = new ArrayList<>();

        textViewCourse.setText(intent.getStringExtra(CrudOperation.COURSE_NAME));


        buttonAddComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveComment();
            }
        });

        listViewComments.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //getting the selected comment
                Comment comment = comments.get(i);

                //creating an intent
                Intent intent = new Intent(getApplicationContext(), CourseStudentCommentActivity.class);

                //putting comment name and id to intent
                intent.putExtra(COMMENT_ID, comment.getId());
                intent.putExtra(COMMENT_NAME, comment.getName());

                //starting the activity with intent
                startActivity(intent);
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();

        databaseComments.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                comments.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Comment comment = postSnapshot.getValue(Comment.class);
                    comments.add(comment);
                }
                CommentList commentListAdapter = new CommentList(CourseStudentCommentActivity.this, comments);
                listViewComments.setAdapter(commentListAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void saveComment() {
        String commentName = editTextCommentName.getText().toString().trim();
        String commentInfo = editTextCommentInfo.getText().toString().trim();
        if (!TextUtils.isEmpty(commentName)) {
            String id  = databaseComments.push().getKey();
            Comment comment = new Comment(id, commentName, commentInfo);
            databaseComments.child(id).setValue(comment);
            Toast.makeText(this, "Comment saved", Toast.LENGTH_LONG).show();
            editTextCommentName.setText("");
        } else {
            Toast.makeText(this, "Please enter comment name", Toast.LENGTH_LONG).show();
        }
    }





















}