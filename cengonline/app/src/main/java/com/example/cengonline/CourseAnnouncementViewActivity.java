package com.example.cengonline;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class CourseAnnouncementViewActivity extends AppCompatActivity {
    public static final String ANNOUNCEMENT_NAME = "com.example.cengonline.announcementname";
    public static final String ANNOUNCEMENT_ID = "com.example.cengonline.announcementid";



    TextView  textViewCourse;
    ListView listViewAnnouncements;

    DatabaseReference databaseAnnouncements;

    List<Announcement> announcements;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_announcement_view);

        Intent intent = getIntent();

        /*
         * this line is important
         * this time we are not getting the reference of a direct node
         * but inside the node announcement we are creating a new child with the course id
         * and inside that node we will store all the announcements with unique ids
         * */
        databaseAnnouncements = FirebaseDatabase.getInstance().getReference("announcements").child(intent.getStringExtra(CrudAnnouncementViewOperation.COURSE_ID));


        textViewCourse = (TextView) findViewById(R.id.textViewCourse);
        listViewAnnouncements = (ListView) findViewById(R.id.listViewAnnouncements);

        announcements = new ArrayList<>();

        textViewCourse.setText(intent.getStringExtra(CrudOperation.COURSE_NAME));




        listViewAnnouncements.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //getting the selected announcement
                Announcement announcement = announcements.get(i);

                //creating an intent
                Intent intent = new Intent(getApplicationContext(), CourseAnnouncementViewActivity.class);

                //putting announcement name and id to intent
                intent.putExtra(ANNOUNCEMENT_ID, announcement.getId());
                intent.putExtra(ANNOUNCEMENT_NAME, announcement.getName());

                //starting the activity with intent
                startActivity(intent);
            }
        });



    }

    @Override
    protected void onStart() {
        super.onStart();

        databaseAnnouncements.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                announcements.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Announcement announcement = postSnapshot.getValue(Announcement.class);
                    announcements.add(announcement);
                }
                AnnouncementList announcementListAdapter = new AnnouncementList(CourseAnnouncementViewActivity.this, announcements);
                listViewAnnouncements.setAdapter(announcementListAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }























}