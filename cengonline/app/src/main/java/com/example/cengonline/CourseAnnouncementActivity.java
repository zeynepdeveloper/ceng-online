package com.example.cengonline;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.List;

public class CourseAnnouncementActivity extends AppCompatActivity {
    public static final String ANNOUNCEMENT_NAME = "com.example.cengonline.announcementname";
    public static final String ANNOUNCEMENT_ID = "com.example.cengonline.announcementid";


    Button buttonAddAnnouncement;
    EditText editTextAnnouncementName;
    EditText editTextAnnouncementInfo;
    TextView  textViewCourse;
    ListView listViewAnnouncements;

    DatabaseReference databaseAnnouncements;

    List<Announcement> announcements;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_announcement);

        Intent intent = getIntent();

        /*
         * this line is important
         * this time we are not getting the reference of a direct node
         * but inside the node announcement we are creating a new child with the course id
         * and inside that node we will store all the announcements with unique ids
         * */
        databaseAnnouncements = FirebaseDatabase.getInstance().getReference("announcements").child(intent.getStringExtra(CrudAnnouncementOperation.COURSE_ID));

        buttonAddAnnouncement = (Button) findViewById(R.id.buttonAddAnnouncement);
        editTextAnnouncementName = (EditText) findViewById(R.id.editTextName);
        editTextAnnouncementInfo = (EditText) findViewById(R.id.editTextInfo);
        textViewCourse = (TextView) findViewById(R.id.textViewCourse);
        listViewAnnouncements = (ListView) findViewById(R.id.listViewAnnouncements);

        announcements = new ArrayList<>();

        textViewCourse.setText(intent.getStringExtra(CrudOperation.COURSE_NAME));


        buttonAddAnnouncement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveAnnouncement();
            }
        });

        listViewAnnouncements.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //getting the selected announcement
                Announcement announcement = announcements.get(i);

                //creating an intent
                Intent intent = new Intent(getApplicationContext(), CourseAnnouncementActivity.class);

                //putting announcement name and id to intent
                intent.putExtra(ANNOUNCEMENT_ID, announcement.getId());
                intent.putExtra(ANNOUNCEMENT_NAME, announcement.getName());

                //starting the activity with intent
                startActivity(intent);
            }
        });

        listViewAnnouncements.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Announcement announcement = announcements.get(i);
                showUpdateDeleteDialog(announcement.getId(), announcement.getName());
                return true;
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        databaseAnnouncements.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                announcements.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Announcement announcement = postSnapshot.getValue(Announcement.class);
                    announcements.add(announcement);
                }
                AnnouncementList announcementListAdapter = new AnnouncementList(CourseAnnouncementActivity.this, announcements);
                listViewAnnouncements.setAdapter(announcementListAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void saveAnnouncement() {
        String announcementName = editTextAnnouncementName.getText().toString().trim();
        String announcementInfo = editTextAnnouncementInfo.getText().toString().trim();
        if (!TextUtils.isEmpty(announcementName)) {
            String id  = databaseAnnouncements.push().getKey();
            Announcement announcement = new Announcement(id, announcementName, announcementInfo);
            databaseAnnouncements.child(id).setValue(announcement);
            Toast.makeText(this, "Announcement saved", Toast.LENGTH_LONG).show();
            editTextAnnouncementName.setText("");
        } else {
            Toast.makeText(this, "Please enter announcement name", Toast.LENGTH_LONG).show();
        }
    }

    private void showUpdateDeleteDialog(final String announcementId, String announcementName) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.update_announcement_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText editTextName = (EditText) dialogView.findViewById(R.id.editTextName);
        final EditText editTextInfo = (EditText) dialogView.findViewById(R.id.editTextInfo);
        final Button buttonUpdate = (Button) dialogView.findViewById(R.id.buttonUpdateAnnouncement);
        final Button buttonDelete = (Button) dialogView.findViewById(R.id.buttonDeleteAnnouncement);

        dialogBuilder.setTitle(announcementName);
        final AlertDialog b = dialogBuilder.create();
        b.show();


        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = editTextName.getText().toString().trim();
                String a_info = editTextInfo.getText().toString().trim();
                if (!TextUtils.isEmpty(name)) {
                    updateAnnouncement(announcementId, name, a_info);
                    b.dismiss();
                }
            }
        });

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                deleteAnnouncement(announcementId);
                b.dismiss();
            }
        });
    }


    private boolean updateAnnouncement(String id, String name, String info) {
        Intent intent = getIntent();
        //getting the specified announcement reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference("announcements").child(intent.getStringExtra(CrudAnnouncementOperation.COURSE_ID));

        DatabaseReference dRA = dR.child(id);

        Announcement announcement = new Announcement(id, name, info);
        dRA.setValue(announcement);
        Toast.makeText(getApplicationContext(), "Announcement Updated", Toast.LENGTH_LONG).show();
        return true;

    }




    private boolean deleteAnnouncement(String id) {
        Intent intent = getIntent();
        //getting the specified announcement reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference("announcements").child(intent.getStringExtra(CrudAnnouncementOperation.COURSE_ID));

        DatabaseReference dRA = dR.child(id);

        //removing announcement
        dRA.removeValue();

        Toast.makeText(getApplicationContext(), "Announcement Deleted", Toast.LENGTH_LONG).show();

        return true;
    }


















}