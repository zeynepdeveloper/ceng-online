package com.example.cengonline;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.List;

public class CourseAssignmentActivity extends AppCompatActivity {

    public static final String ASSIGNMENT_NAME = "com.example.cengonline.assignmentname";
    public static final String ASSIGNMENT_ID = "com.example.cengonline.assignmentid";

    Button buttonAddAssignment;
    EditText editTextAssignmentName, editTextAssignmentInfo, editTextAssignmentDueDate;

    TextView  textViewCourse;
    ListView listViewAssignments;

    DatabaseReference databaseAssignments;

    List<Assignment> assignments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course);

        Intent intent = getIntent();

        /*
         * this line is important
         * this time we are not getting the reference of a direct node
         * but inside the node assignment we are creating a new child with the course id
         * and inside that node we will store all the assignments with unique ids
         * */
        databaseAssignments = FirebaseDatabase.getInstance().getReference("assignments").child(intent.getStringExtra(CrudOperation.COURSE_ID));

        buttonAddAssignment = (Button) findViewById(R.id.buttonAddAssignment);
        editTextAssignmentName = (EditText) findViewById(R.id.editTextName);
        editTextAssignmentInfo = (EditText) findViewById(R.id.editTextInfo);
        editTextAssignmentDueDate = (EditText) findViewById(R.id.editTextDueDate);

        textViewCourse = (TextView) findViewById(R.id.textViewCourse);
        listViewAssignments = (ListView) findViewById(R.id.listViewAssignments);

        assignments = new ArrayList<>();

        textViewCourse.setText(intent.getStringExtra(CrudOperation.COURSE_NAME));


        buttonAddAssignment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveAssignment();
            }
        });

        listViewAssignments.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //getting the selected assignment
                Assignment assignment = assignments.get(i);

                //creating an intent
                Intent intent = new Intent(getApplicationContext(), CourseAssignmentActivity.class);

                //putting assignment name and id to intent
                intent.putExtra(ASSIGNMENT_ID, assignment.getId());
                intent.putExtra(ASSIGNMENT_NAME , assignment.getName());

                //starting the activity with intent
                startActivity(intent);
            }
        });

        listViewAssignments.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Assignment assignment = assignments.get(i);
                showUpdateDeleteDialog(assignment.getId(), assignment.getName());
                return true;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        databaseAssignments.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                assignments.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Assignment assignment = postSnapshot.getValue(Assignment.class);
                    assignments.add(assignment);
                }
                AssignmentList assignmentListAdapter = new AssignmentList(CourseAssignmentActivity.this, assignments);
                listViewAssignments.setAdapter(assignmentListAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void saveAssignment() {
        String assignmentName = editTextAssignmentName.getText().toString().trim();
        String assignmentInfo = editTextAssignmentInfo.getText().toString().trim();
        String assignmentDueDate = editTextAssignmentDueDate.getText().toString().trim();

        if (!TextUtils.isEmpty(assignmentName)) {
            String id  = databaseAssignments.push().getKey();
            Assignment assignment = new Assignment(id, assignmentName, assignmentInfo, assignmentDueDate);
            databaseAssignments.child(id).setValue(assignment);
            Toast.makeText(this, "Assignment saved", Toast.LENGTH_LONG).show();
            editTextAssignmentName.setText("");
        } else {
            Toast.makeText(this, "Please enter assignment name", Toast.LENGTH_LONG).show();
        }
    }


    private void showUpdateDeleteDialog(final String assignmentId, String assignmentName) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.update_assignment_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText editTextName = (EditText) dialogView.findViewById(R.id.editTextName);
        final EditText editTextInfo = (EditText) dialogView.findViewById(R.id.editTextInfo);
        final EditText editTextDuedate = (EditText) dialogView.findViewById(R.id.editTextDueDate);
        final Button buttonUpdate = (Button) dialogView.findViewById(R.id.buttonUpdateAssignment);
        final Button buttonDelete = (Button) dialogView.findViewById(R.id.buttonDeleteAssignment);

        dialogBuilder.setTitle(assignmentName);
        final AlertDialog b = dialogBuilder.create();
        b.show();


        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = editTextName.getText().toString().trim();
                String a_info = editTextInfo.getText().toString().trim();
                String duedate = editTextDuedate.getText().toString().trim();
                if (!TextUtils.isEmpty(name)) {
                    updateAssignment(assignmentId, name, a_info, duedate);
                    b.dismiss();
                }
            }
        });

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                deleteAssignment(assignmentId);
                b.dismiss();
            }
        });
    }


    private boolean updateAssignment(String id, String name, String info, String duedate) {
        Intent intent = getIntent();
        //getting the specified assignment reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference("assignments").child(intent.getStringExtra(CrudOperation.COURSE_ID));

        DatabaseReference dRA = dR.child(id);

        Assignment assignment = new Assignment(id, name, info, duedate);
        dRA.setValue(assignment);
        Toast.makeText(getApplicationContext(), "Assignment Updated", Toast.LENGTH_LONG).show();
        return true;

    }


    private boolean deleteAssignment(String id) {
        Intent intent = getIntent();
        //getting the specified assignment reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference("assignments").child(intent.getStringExtra(CrudOperation.COURSE_ID));

        DatabaseReference dRA = dR.child(id);

        //removing assignment
        dRA.removeValue();

        Toast.makeText(getApplicationContext(), "Assignment Deleted", Toast.LENGTH_LONG).show();

        return true;
    }









}