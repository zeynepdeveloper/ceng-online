package com.example.cengonline;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        new MyThread().start();
    }
    public class MyThread extends Thread{
        public void run(){
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                Toast.makeText(MainActivity2.this, e.toString(), Toast.LENGTH_SHORT).show();
            }
            finally {
                Intent intent = new Intent(MainActivity2.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }
}
