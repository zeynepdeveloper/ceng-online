package com.example.cengonline;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class SubmittedWork extends ModuleInformation {
    // CONSTRUCTORS
    public SubmittedWork(String id, String name, String info) {
        super(id, name, info);
    }

    public SubmittedWork() {
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
