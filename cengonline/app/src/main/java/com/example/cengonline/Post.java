package com.example.cengonline;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Post extends ModuleInformation {
    private String dueDate;

    // CONSTRUCTORS
    public Post(String id, String name, String info, String dueDate) {
        super(id, name, info);
        this.dueDate=dueDate;
    }
    public Post() {
    }

    public Post(String id, String name) {
        super(id, name);
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }


    @Override
    public String toString() {
        return "Post{" + super.toString()+
                "dueDate='" + dueDate + '\'' +
                '}';
    }
}
