package com.example.cengonline;

public abstract class ModuleInformation {
    private String id;
    private String name;
    private String info;

    public ModuleInformation() {
    }

    public ModuleInformation(String id,String name,String info) {
        this.id = id;
        this.name=name;
        this.info=info;
    }

    public String getId() {
        return id;
    }

    public ModuleInformation(String id,String name) {
        this.id = id;
        this.name=name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public  String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "ModuleInformation{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", info='" + info + '\'' +
                '}';
    }
}
