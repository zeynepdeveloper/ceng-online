package com.example.cengonline;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

//import android.support.annotation.NonNull;


public class SubmittedWorkList extends ArrayAdapter<SubmittedWork> {
    private Activity context;
    List<SubmittedWork> submittedWorks;

    public SubmittedWorkList(Activity context, List<SubmittedWork> submittedWorks) {
        super(context, R.layout.layout_submitted_work_list, submittedWorks);
        this.context = context;
        this.submittedWorks = submittedWorks;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.layout_submitted_work_list, null, true);

        TextView textViewName = (TextView) listViewItem.findViewById(R.id.textViewName);
        TextView textViewGenre = (TextView) listViewItem.findViewById(R.id.textViewInfo);

        SubmittedWork submittedWork = submittedWorks.get(position);
        textViewName.setText(submittedWork.getName());
        textViewGenre.setText(submittedWork.getInfo());

        return listViewItem;
    }
}