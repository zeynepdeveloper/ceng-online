Firebase and Android Studio Project

This application facilitates communication between students and teachers. Students easily follow the lessons. 
The main reason and focus behind the idea of this app is based around the user’s follow the lessons and teacher's follow the students.


 Functional Requirements

    Login:

     Two types of users can log into the CENG Online course management system:  Teachers and Students.

    Courses

     Teachers can add/edit/delete a course. Students can only view     the courses they have enrolled in.

    Assignments:

     Teachers can add/edit/delete an assignment and can view submitted works by    students. Students can only view the assignments and upload their works (only text submission, no file upload required).

    Announcements:

     Teachers can add/edit/delete announcements. Students can only view the announcements.

    Messaging:

      All the system users can send individual messages to each other.

    Stream:

      Each course has an own stream. Participants can communicate on the stream page using posts and comments. Teachers can add/edit/delete posts. Teachers and students can send comments as response to a post.
